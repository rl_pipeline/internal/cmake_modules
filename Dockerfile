FROM registry.gitlab.com/rl_pipeline/internal/docker/centos-build:0.1.0 as centos-build

WORKDIR /home/dev

COPY . ./cmake_modules

RUN cd /home/dev/cmake_modules && mkdir build \
    && cd build \
    && cmake .. \
    && make \ 
    && make install

FROM centos:centos8
COPY --from=centos-build /usr/local/share/cmake_modules /usr/local/share/cmake_modules

ARG CMAKE_MODULE
ENV CMAKE_MODULE_PATH=${CMAKE_MODULE}:${CMAKE_MODULE_PATH}