#!/bin/sh
docker build \
    --build-arg CMAKE_MODULE=/usr/local/share/cmake_modules/share/cmake_modules \
    --no-cache \
    --tag $1:$2 \
    .