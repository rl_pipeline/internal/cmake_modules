# -----------------------------------------------------------------------------
#
# Module contains helpers macro and function to get/download a file. 
#
# Functions:
#   stage_files    - Staging the given filepaths into the destination path.
#                       
# -----------------------------------------------------------------------------


include(CMakeParseArguments)
include(PathUtils)


# -----------------------------------------------------------------------------
# Private macro to set a root payload path. 
#
# REQUIRED PARAMETERS:
#   PAYLOAD_PATH
#     Output variable to store a payload path.
#
# -----------------------------------------------------------------------------
macro(_SET_PAYLOAD_PATH PAYLOAD_PATH)
    # Check if user sets a CUSTOM_PAYLOAD_PATH cached variable.
    if(DEFINED CUSTOM_PAYLOAD_PATH)
        set(${PAYLOAD_PATH} ${CUSTOM_PAYLOAD_PATH})
    # Check if user sets an environment variable REZ_REPO_PAYLOAD_DIR.
    # This was meant to be used with rez package workflow.
    elseif(DEFINED ENV{REZ_REPO_PAYLOAD_DIR})
        set(${PAYLOAD_PATH} $ENV{REZ_REPO_PAYLOAD_DIR})
    else()
        message(FATAL_ERROR "Missing REZ_REPO_PAYLOAD_DIR env variable or CUSTOM_PAYLOAD_PATH cached variable!")
    endif()
endmacro(set_payload)


# -----------------------------------------------------------------------------
# Get a file from the given RELATIVE_FILEPATH. If doesn't exists, download the 
# file from the given URL. Finally, set the file path to ARCHIVE_FILEPATH on the 
# parent scope.
#
# REQUIRED PARAMETERS:
#   RELATIVE_FILEPATH
#     File path which is relative to either CUSTOM_PAYLOAD_PATH or REZ_REPO_PAYLOAD_DIR.
#
#   URL
#     URL address to download the file if the given RELATIVE_FILEPATH doesn't
#     exist.
#
# -----------------------------------------------------------------------------
function(get_or_download_file)
    set(singleValues RELATIVE_FILEPATH URL)

    include(CMakeParseArguments)
    cmake_parse_arguments(
        RL
        "" 
        "${singleValues}" 
        ""
        ${ARGN}
    )
    _SET_PAYLOAD_PATH(PAYLOAD_PATH)

    set(archive_filepath "${PAYLOAD_PATH}/${RL_RELATIVE_FILEPATH}")
    
    if(EXISTS ${archive_filepath} AND NOT IS_DIRECTORY ${archive_filepath})
        message("-- ARCHIVE EXISTS: ${archive_filepath}")
        execute_process(
            COMMAND ${BASH} du ${archive_filepath}
            COMMAND ${BASH} cut "-f1"
            OUTPUT_VARIABLE raw_file_size
        )
        
        if(${raw_file_size} EQUAL "0")
            file(REMOVE ${archive_filepath})
            message("-- FILE SIZE IS ZERO KILOBYTES. DELETED EXISTING FILE.")
        endif()
    endif()

    if(DEFINED RL_URL AND NOT EXISTS ${archive_filepath})
        get_filename_component(archive_path ${archive_filepath} DIRECTORY)
        validate_directory(DIRECTORY_PATH ${archive_path})

        message("-- DOWNLOAD:")
        message("--   FILE: ${archive_filepath}")
        message("--   SOURCE: ${RL_URL}")
        file(
            DOWNLOAD ${RL_URL} ${archive_filepath} 
            SHOW_PROGRESS
            STATUS statuses
            TIMEOUT 300
        )
        list(GET statuses 0 status_number)
        if(NOT ${status_number} EQUAL 0)
            message(FATAL_ERROR "FAILED TO DOWNLOAD FROM: ${RL_URL}")
        else()
            message("-- DOWNLOAD SUCCEED: ${archive_filepath}")
        endif()
    endif()
    set(ARCHIVE_FILEPATH ${archive_filepath} PARENT_SCOPE)
endfunction()