# -----------------------------------------------------------------------------
#
# Module contains helpers macro to configure and install pkgconfig.
#
# Macros:
#   CONFIGURE_AND_INSTALL_PKGCONFIG - Configure and install pkgconfig.
#                       
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# Configure a pkgconfig cmake.in file from the given INPUT_CONFIG and install 
# the configured file to the given OUTPUT_CONFIG.
#
# REQUIRED PARAMETERS:
#   INPUT_CONFIG
#     A pkgconfig cmake.in file. This is relative to CMAKE_CURRENT_BINARY_DIR.
#
#   OUTPUT_CONFIG
#     An install file path for the pkgconfig .pc file.
#
# -----------------------------------------------------------------------------
macro(CONFIGURE_AND_INSTALL_PKGCONFIG INPUT_CONFIG OUTPUT_CONFIG)
    configure_file(
        ${INPUT_CONFIG}
        ${CMAKE_CURRENT_BINARY_DIR}/pkgconfig.cmake
        ESCAPE_QUOTES
        @ONLY
    )
    install(SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/pkgconfig.cmake)
    install(
        FILES 
            ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc
        DESTINATION
            ${OUTPUT_CONFIG}
    )
endmacro(CONFIGURE_AND_INSTALL_PKGCONFIG)