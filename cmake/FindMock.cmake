# -----------------------------------------------------------------------------
#
# Module contains a helper to find Mock python-2 module.
#
# First of all, it will try to find the python-2 interpreter and components location.
#
# If MOCK_ROOT location is defined by user as cached variable, call the pkg_check_modules
# to search the pkgconfig .pc file and get the information from the file. If
# the pkg_check_modules failed to find the MOCK, then try to call the find_path
# from the site package directory relatived to the given MOCK_ROOT.
#
# If MOCK_ROOT location is not defined, get the default python user site package
# directory from the python interpreter. Call the find_path using few hints below:
#   - ${PYTHON_USER_SITE_PACKAGES}/mock
#   - /usr/lib/python${major_minor}/site-packages/mock
#   - /usr/local/python/lib/python${major_minor}/site-packages/mock
#
# RESULT_ENVIRONMENT_VARIABLES:
#   MOCK_LIBDIR
#     Mock python-2 module installation library directory.
#
# -----------------------------------------------------------------------------


include(PathUtils)


find_package(Python2 COMPONENTS Interpreter Development)
set(python_version_major ${Python2_VERSION_MAJOR})
set(python_version_minor ${Python2_VERSION_MINOR})
set(major_minor ${python_version_major}.${python_version_minor})


if(DEFINED MOCK_ROOT)
    find_package(PkgConfig REQUIRED)

    set(ENV{PKG_CONFIG_PATH} ${MOCK_ROOT}/lib/pkgconfig)
    pkg_check_modules(MOCK QUIET mock)
    if(${MOCK_FOUND})
        set(ENV{MOCK_LIBDIR} ${MOCK_LIBDIR})
    else()
        set(site_package_dir ${MOCK_ROOT}/lib/python${major_minor}/site-packages)
        find_path(
            mock_module 
            NAMES mock.py 
            HINTS ${site_package_dir}/mock
        )
        set(ENV{MOCK_LIBDIR} ${mock_module})
    endif()
else()
    get_python2_user_site_pkg_directory()
    find_path(
        mock_module 
        NAMES   mock.py 
        HINTS   ${PYTHON2_USER_SITE_PACKAGES}/mock
                /usr/lib/python${major_minor}/site-packages/mock
                /usr/local/python/lib/python${major_minor}/site-packages/mock
    )
    set(ENV{MOCK_LIBDIR} ${mock_module})
endif()


if($ENV{MOCK_LIBDIR} STREQUAL "mock_module-NOTFOUND")
    message(FATAL_ERROR "Failed to find mock module!")
endif()