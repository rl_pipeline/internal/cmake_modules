# -----------------------------------------------------------------------------
#
# Module contains helpers functions to handle variable/cache variable operations. 
#
# Functions:
#   collect_cached_variables - Collect arguments (-D) cached variables
#
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# Collect arguments (-D) cached variables.
#
# OPTIONAL PARAMETERS:
#   EXTRA_VARS
#     Extra cached variables.
#
# RESULT VARIABLES:
#   CMAKE_ARGS
#     List of arguments (-D) cached variables.
# -----------------------------------------------------------------------------
function(collect_cached_variables)
    set(multiValueArgs EXTRA_VARS)
    include(CMakeParseArguments)
    cmake_parse_arguments(
        RL
        ""
        ""
        "${multiValueArgs}"
        ${ARGN}
    )
    get_cmake_property(CACHE_VARS CACHE_VARIABLES)
    foreach(CACHE_VAR ${CACHE_VARS})
        get_property(CACHE_VAR_HELPSTRING CACHE ${CACHE_VAR} PROPERTY HELPSTRING)
        if(CACHE_VAR_HELPSTRING STREQUAL "No help, variable specified on the command line.")
            get_property(CACHE_VAR_TYPE CACHE ${CACHE_VAR} PROPERTY TYPE)
            if(CACHE_VAR_TYPE STREQUAL "UNINITIALIZED")
                set(CACHE_VAR_TYPE)
            else()
                set(CACHE_VAR_TYPE :${CACHE_VAR_TYPE})
            endif()
            list(APPEND _CMAKE_ARGS "-D${CACHE_VAR}${CACHE_VAR_TYPE}=${${CACHE_VAR}}")
        endif()
    endforeach()
    list(APPEND _CMAKE_ARGS ${RL_EXTRA_VARS})
    set(CMAKE_ARGS ${_CMAKE_ARGS} PARENT_SCOPE)
endfunction(Variable_CollectCacheVariables)