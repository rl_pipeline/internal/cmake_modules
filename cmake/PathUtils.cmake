# -----------------------------------------------------------------------------
#
# Module contains helpers functions to handle directory operations. 
#
# Functions:
#   validate_directory                   - Check if the given DIRECTORY_PATH exists. 
#                                          If not exists, create the directory.
#   get_python2_user_site_pkg_directory  - Get python user site-package directory.
#   get_python2_user_base_directory      - Get python user base directory.
#                       
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# Check if the given DIRECTORY_PATH exists. If not exists, create the directory.
#
# REQUIRED PARAMETERS:
#   DIRECTORY_PATH
#     Directory path to validate.
#
# -----------------------------------------------------------------------------
function(validate_directory)
    set(singleValues DIRECTORY_PATH)
    include(CMakeParseArguments)
    cmake_parse_arguments(
        RL
        "" 
        "${singleValues}" 
        ""
        ${ARGN}
    )

    if(NOT EXISTS ${RL_DIRECTORY_PATH})
        message("-- CREATE DIRECTORY: ${RL_DIRECTORY_PATH}")
        file(MAKE_DIRECTORY ${RL_DIRECTORY_PATH})
    endif()
endfunction()


# -----------------------------------------------------------------------------
# Get python-2 user site-package directory from the python-2 interpreter.
#
# RESULT VARIABLES
#   PYTHON2_USER_SITE_PACKAGES
#     Python user site-package directory path.
#
# -----------------------------------------------------------------------------
function(get_python2_user_site_pkg_directory)
    find_package(Python2 COMPONENTS Interpreter Development)
    execute_process(
        COMMAND ${Python2_EXECUTABLE} -c "import site
print(site.getusersitepackages())"
        OUTPUT_VARIABLE OUTPUT_DIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    set(PYTHON2_USER_SITE_PACKAGES ${OUTPUT_DIR} PARENT_SCOPE)
endfunction()


# -----------------------------------------------------------------------------
# Get python-2 user base directory from the python-2 interpreter.
#
# RESULT VARIABLES
#   PYTHON2_USER_BASE
#     Python user site-package directory path.
#
# -----------------------------------------------------------------------------
function(get_python2_user_base_directory)
    find_package(Python2 COMPONENTS Interpreter Development)
    execute_process(
        COMMAND ${Python2_EXECUTABLE} -c "import site
print(site.getuserbase())"
        OUTPUT_VARIABLE OUTPUT_DIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    set(PYTHON2_USER_BASE ${OUTPUT_DIR} PARENT_SCOPE)
endfunction()
