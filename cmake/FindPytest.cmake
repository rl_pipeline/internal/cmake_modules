# -----------------------------------------------------------------------------
#
# Module contains a helper to find Pytest python-2 module.
#
# First of all, it will try to find the python-2 interpreter and components location.
#
# If PYTEST_ROOT location is defined by user as cached variable, call the pkg_check_modules
# to search the pkgconfig .pc file and get the information PYTEST_LIBDIR from the file. 
# If the pkg_check_modules failed to find the Pytest, then try to call the find_path
# from the site package directory relatived to the given PYTEST_ROOT.
#
# If PYTEST_ROOT location is not defined, get the default python user site package
# directory from the python interpreter. Call the find_path using few hints below:
#   - ${PYTHON_USER_SITE_PACKAGES}
#   - /usr/lib/python${major_minor}/site-packages
#   - /usr/local/python/lib/python${major_minor}/site-packages
#
#
# RESULT_ENVIRONMENT_VARIABLES:
#   PYTEST_PREFIX
#     Pytest python-2 module installation root/prefix directory.
#
#   PYTEST_EXECUTABLE
#     Pytest python-2 module executable file.
#
#   PYTEST_VERSION
#     Pytest python-2 module version.
#
#   PYTEST_LIBDIR
#     Pytest python-2 module installation library directory.
#
# -----------------------------------------------------------------------------


include(PathUtils)

# Find python-2 package. This is required before findind pytest.
find_package(Python2 COMPONENTS Interpreter Development)
set(python_version_major ${Python2_VERSION_MAJOR})
set(python_version_minor ${Python2_VERSION_MINOR})
set(major_minor ${python_version_major}.${python_version_minor})

if(DEFINED PYTEST_ROOT)
    # If PYTEST_ROOT cache variable is defined, run pkg_check_modules at first.
    set(ENV{PKG_CONFIG_PATH} ${PYTEST_ROOT}/lib/pkgconfig)
    pkg_check_modules(PYTEST QUIET pytest)
    if(${PYTEST_FOUND})
        set(ENV{PYTEST_LIBDIR} ${PYTEST_LIBDIR})
    else()
        # If pkg_check_modules failed, do find pytest.py file manually.
        set(site_package_dir ${PYTEST_ROOT}/lib/python${major_minor}/site-packages)
        find_path(
            pytest_module 
            NAMES pytest.py 
            HINTS ${site_package_dir}
        )
        set(ENV{PYTEST_LIBDIR} ${pytest_module})
    endif()
else()
    # Try to find pytest.py using python site module.
    get_python2_user_site_pkg_directory()
    find_path(
        pytest_module 
        NAMES   pytest.py 
        HINTS   ${PYTHON2_USER_SITE_PACKAGES}
                /usr/lib/python${major_minor}/site-packages
                /usr/local/python/lib/python${major_minor}/site-packages
    )
    set(ENV{PYTEST_LIBDIR} ${pytest_module})
endif()

# Check and validate if PYTEST_LIBDIR was successfully set.
if($ENV{PYTEST_LIBDIR} STREQUAL "pytest_module-NOTFOUND")
    message(FATAL_ERROR "Failed to find pytest module!")
endif()

# Find pytest executable
get_python2_user_base_directory()
find_path(
    pytest_bin 
    NAMES   pytest 
    HINTS   ${PYTHON2_USER_BASE}/bin
            ${PYTEST_ROOT}/bin
            /usr/bin
            /usr/local/python/bin
)
set(ENV{PYTEST_EXECUTABLE} ${pytest_bin}/pytest)

# Get pytest version
execute_process(
        COMMAND ${Python2_EXECUTABLE} -c "import pytest
print(pytest.__version__)"
        OUTPUT_VARIABLE PYTEST_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE
)
set(ENV{PYTEST_VERSION} ${PYTEST_VERSION})

# Get pytest prefix
get_filename_component(pytest_prefix ${pytest_bin} DIRECTORY)
set(ENV{PYTEST_PREFIX} ${pytest_prefix})

# Print status
message(STATUS "Found pytest: $ENV{PYTEST_EXECUTABLE}(found version \"$ENV{PYTEST_VERSION}\")")