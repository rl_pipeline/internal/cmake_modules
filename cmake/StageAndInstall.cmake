# -----------------------------------------------------------------------------
#
# Module contains helpers functions and variables used for staging and install
# files.
#
# Functions:
#   stage_files    - Staging the given filepaths into the destination path.
#
# Variables:
#   RL_FILE_DEFAULT_PERMISSIONS         - Default file permissions.
#   RL_DIRECTORY_DEFAULT_PERMISSIONS    - Default directory permissions.
#   RL_EXECUTABLE_DEFAULT_PERMISSIONS   - Default excutable file permissions.
#   RL_INSTALL_LIBDIR                   - Default lib directory name (GNUInstallDirs)
#   RL_INSTALL_BINDIR                   - Default bin directory name (GNUInstallDirs)
#   RL_STAGING_DIR                      - Default staging directory path, relative to
#                                         CMAKE_BINARY_DIR
#                       
# -----------------------------------------------------------------------------


include(GNUInstallDirs)

# Set default permissions
set(RL_FILE_DEFAULT_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ)
set(RL_DIRECTORY_DEFAULT_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ GROUP_WRITE WORLD_READ)
set(
    RL_EXECUTABLE_DEFAULT_PERMISSIONS 
    ${RL_FILE_DEFAULT_PERMISSIONS} OWNER_EXECUTE GROUP_EXECUTE
)


# We give a chance to user for overriding the GNU default directories
set(
    RL_INSTALL_LIBDIR 
    ${CMAKE_INSTALL_LIBDIR} 
    CACHE PATH "Installation directory for libraries"
)
set(
    RL_INSTALL_BINDIR 
    ${CMAKE_INSTALL_BINDIR} 
    CACHE PATH "Installation directory for executables"
)


# Added default staging directory
set(
    RL_STAGING_DIR
    ${CMAKE_BINARY_DIR}/stage
)


# -----------------------------------------------------------------------------
# Staging the given filepaths into the destination path. 
#
# REQUIRED PARAMETERS:
#   FILEPATHS
#     File paths to copy to the staging directory. 
#
#   DESTINATION
#     Destination path for copying to. By default, destination path is relative 
#     to the RL_STAGING_DIR. It can also support absolute file path.
#
# OPTIONAL PARAMETERS:
#   CREATE_SYMLINKS
#     Instead of copy, create symlink files to the staging directory for each 
#     given file paths.
#
#   EXECUTABLE
#     Set given file paths with RL_EXECUTABLE_DEFAULT_PERMISSIONS.
#
# -----------------------------------------------------------------------------
function(stage_files)
    set(options CREATE_SYMLINKS EXECUTABLE)
    set(singleValueArgs DESTINATION)
    set(multiValueArgs FILEPATHS)
    include(CMakeParseArguments)
    cmake_parse_arguments(
        STAGE_FILES
        "${options}"
        "${singleValueArgs}"
        "${multiValueArgs}"
        ${ARGN}
    )
    
    # Handle if a relative destination filepath is given
    if(NOT IS_ABSOLUTE ${STAGE_FILES_DESTINATION})
        set(dest_abs_root ${RL_STAGING_DIR}/${STAGE_FILES_DESTINATION})
    else()
        set(dest_abs_root ${STAGE_FILES_DESTINATION})
    endif()

    foreach(STAGE_FILES_FILEPATH ${STAGE_FILES_FILEPATHS})
        file(RELATIVE_PATH rel_filepath ${CMAKE_CURRENT_SOURCE_DIR} ${STAGE_FILES_FILEPATH})
        # Trim out first directory of the relative path
        string(REGEX REPLACE "^[a-zA-Z0-9_\.-]+" "" rel_filepath_edit ${rel_filepath})
        set(destination_filepath ${dest_abs_root}/${rel_filepath_edit})
        get_filename_component(destination_dir ${destination_filepath} DIRECTORY)
        if(${STAGE_FILES_CREATE_SYMLINKS})
            message(
                STATUS 
                "Staging (symlink) : ${destination_filepath} -> ${STAGE_FILES_FILEPATH}"
            )
            execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${destination_dir})
            execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink ${STAGE_FILES_FILEPATH} ${destination_filepath})
        else()
            if(STAGE_FILES_EXECUTABLE)
                set(permission ${RL_EXECUTABLE_DEFAULT_PERMISSIONS})
            else()
                set(permission ${RL_FILE_DEFAULT_PERMISSIONS})
            endif()
            get_filename_component(filename ${STAGE_FILES_FILEPATH} NAME)
            message(
                STATUS 
                "Staging (copy) : ${STAGE_FILES_FILEPATH} -> ${destination_dir}/${filename}"
            )
            file(
                COPY
                    ${STAGE_FILES_FILEPATH}
                DESTINATION
                    ${destination_dir}
                FILE_PERMISSIONS 
                    ${permission}
                DIRECTORY_PERMISSIONS
                    ${RL_DIRECTORY_DEFAULT_PERMISSIONS}
            )
        endif()
    endforeach()
endfunction()
