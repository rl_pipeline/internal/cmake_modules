# [cmake_modules](https://gitlab.com/rl_pipeline/internal/cmake_modules)

### Overview
This package is a set of utility macros, functions and variables to help some automation
of generating a build system, configuration, staging and installation files.

### Disclaimer
This was built under:
* GNU Make 4.1
* Ubuntu 18.04.3 LTS
* cmake-3.14.7
* docker-19.03.12

### Prerequisites
* cmake-3.10+<4
* docker
* make

### How to build docker image

```bash
git clone https://gitlab.com/rl_pipeline/internal/cmake_modules.git
cd cmake_modules
git checkout tags/<tagname>
./build_dockerfile.sh [repository] [tag]
```

### How to run docker image

```bash
docker run -it [repository]:[tag]
```